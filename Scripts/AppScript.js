﻿var matrix_input = new Array(4);
var inverse_matrix = new Array(4);
var det = -1;

var Letters = new Array(26);
Letters[0] = "a";
Letters[1] = "b";
Letters[2] = "c";
Letters[3] = "d";
Letters[4] = "e";
Letters[5] = "f";
Letters[6] = "g";
Letters[7] = "h";
Letters[8] = "i";
Letters[9] = "j";
Letters[10] = "k";
Letters[11] = "l";
Letters[12] = "m";
Letters[13] = "n";
Letters[14] = "o";
Letters[15] = "p";
Letters[16] = "q";
Letters[17] = "r";
Letters[18] = "s";
Letters[19] = "t";
Letters[20] = "u";
Letters[21] = "v";
Letters[22] = "w";
Letters[23] = "x";
Letters[24] = "y";
Letters[25] = "z";

var planeText = [];
var cypherText = [];
var indexes = [];
var C_indexes = [];
var P_indexes = [];


var IsTextisOdd = false;
//var IsLastTextisOdd = false;

$(document).ready(function () {

    $('.Num').bind('keypress', function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
            evt.preventDefault();
        }
    });


    //$('.Num').bind('click', function (evt) {
    //    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
    //        evt.preventDefault();
    //    }
    //});

    $('#Text').keyup(function () {
        if (!(/^[a-z]+$/.test($('#Text').val()))) {
            $("#Next2").attr("disabled", true);
            $("#TextValidation").attr("hidden", false);

        } else {
            $("#Next2").attr("disabled", false);
            $("#TextValidation").attr("hidden", true);
        }
    });

    $("#checkKey").click(function () {

        if ($("#Num00").val() == "" ||
            $("#Num01").val() == "" ||
            $("#Num02").val() == "" ||
            $("#Num03").val() == "") {
            $("#KeysNotOK").attr("hidden", false);
            $("#KeyIsOK").attr("hidden", true);
            $("#NotValid").attr("hidden", true);
        } else {
            //store KEY elements
            matrix_input[0] = $("#Num00").val();
            matrix_input[1] = $("#Num01").val();
            matrix_input[2] = $("#Num02").val();
            matrix_input[3] = $("#Num03").val();

            if (HazNonZeroDet() && HazInverse()) {
                $("#KeysNotOK").attr("hidden", true);
                $("#KeyIsOK").attr("hidden", false);
                $("#NotValid").attr("hidden", true);
                $("#Next1").attr("disabled", false);

                //calculateInverseMatrix();
            } else {
                $("#KeysNotOK").attr("hidden", true);
                $("#KeyIsOK").attr("hidden", true);
                $("#NotValid").attr("hidden", false);

            }
        }


    });

    $('#Next2').on('click',
        function (e) {

            if ($("input[value=Encryption]").is(':checked')) {

                indexes = [];
                P_indexes = [];
                C_indexes = [];
                planeText = [];
                cypherText = [];

                getPlaneText();
                GetLettersIndexesOfText();
                Encryption();
                setCipherText();
            } else {
                //
                indexes = [];
                P_indexes = [];
                C_indexes = [];
                planeText = [];
                cypherText = [];

                calculateInverseMatrix();
                getCipherText();
                getLettersIndexesoOfCipher();
                Decryption();
                setPlaneText();
            }
            $('.btn-circle.btn-primary').removeClass('btn-primary').addClass('btn-default');
            var $activeTab = $('.tab-pane.active');
            var nextTab = $activeTab.next('.tab-pane').attr('id');
            $('[href="#' + nextTab + '"]').addClass('btn-primary').removeClass('btn-default');
            $('[href="#' + nextTab + '"]').removeAttr('disabled');
            $('[href="#' + nextTab + '"]').tab('show');

        });

    $(document).ready(function () {
        $("#cancel").click(function () {

            $("#p1").prop("disabled", false);
            $("#p2").prop("disabled", false);
            $("#p3").prop("disabled", false);
            $("#p4").prop("disabled", false);
            $("#submitKey").attr("disabled", false);

            $("#dropdownMenu").prop("disabled", true);
            $("#text").prop("disabled", true);
            $("#cancel").attr("disabled", true);
            $("#submittext").attr("disabled", true);
        });
    });

    $(document).ready(function () {
        $("#Encrypt").click(function () {
            $("#dropdownMenu").text("Encrypt");

        });
        $("#Decrypt").click(function () {
            $("#dropdownMenu").text("Decrypt");

        });
    });

    $(document).ready(function () {
        $("#submittext").click(function () {
            if ($("#dropdownMenu").text() == ("Encrypt")) {
                indexes = [];
                P_indexes = [];
                C_indexes = [];
                planeText = [];
                cypherText = [];


                getPlaneText();
                getLettersIndexesoOfText();
                Encryption();
                setCipherText();
            } else if ($("#dropdownMenu").text() == ("Decrypt")) {
                /*
                                       for(i =0; i<indexes.length;i++){
                                           indexes[i].clear;
                                       }
                                       for(i =0; i<P_indexes.length;i++){
                                           P_indexes[i].clear;
                                       }
                                       for(i =0; i<C_indexes.length;i++){
                                           C_indexes[i].clear;
                                       }
                                       for(i =0; i<planeText.length;i++){
                                           planeText[i].clear;
                                       }
                                       for(i =0; i<cypherText.length;i++){
                                           cypherText[i].clear;
                                       }*/

                indexes = [];
                P_indexes = [];
                C_indexes = [];
                planeText = [];
                cypherText = [];

                calculateInverseMatrix();
                getCipherText();
                getLettersIndexesoOfCipher();
                Decryption();
                setPlaneText();
            }

        });
    });

    $(document).ready(function () {
        $('.btn-circle').on('click',
            function () {
                $('.btn-circle.btn-primary').removeClass('btn-primary').addClass('btn-default');
                $(this).addClass('btn-primary').removeClass('btn-default').blur();
            });

        $('#Next1').on('click',
            function (e) {
                $('.btn-circle.btn-primary').removeClass('btn-primary').addClass('btn-default');

                var $activeTab = $('.tab-pane.active');
                var nextTab = $activeTab.next('.tab-pane').attr('id');
                $('[href="#' + nextTab + '"]').addClass('btn-primary').removeClass('btn-default');
                $('[href="#' + nextTab + '"]').removeAttr('disabled');
                $('[href="#' + nextTab + '"]').tab('show');

            });


        $('.prev-step').on('click',
            function (e) {
                var $activeTab = $('.tab-pane.active');

                $('.btn-circle.btn-primary').removeClass('btn-primary').addClass('btn-default');


                var prevTab = $activeTab.prev('.tab-pane').attr('id');
                $('[href="#' + prevTab + '"]').addClass('btn-primary').removeClass('btn-default');
                $('[href="#' + prevTab + '"]').tab('show');

            });
    });

    function HazNonZeroDet() {
        var flage = false;
        if ((matrix_input[0] * matrix_input[3]) - (matrix_input[1] * matrix_input[2]) != 0) {
            det = ((matrix_input[0] * matrix_input[3]) - (matrix_input[1] * matrix_input[2]));
            //if det is positive and larger than 26
            if (det > 26) {
                det = det % 26;
                flage = true;
            }
            //if det is negative
            else {
                det = ((det % 26) + 26) % 26;
                flage = true;
            }
        } else {
            flage = false;
        }
        return flage;
    }

    function HazInverse() {
        var flage = false;
        switch (det) {
            case 1:
                det = 1;
                flage = true;
                break;
            case 3:
                det = 9;
                flage = true;
                break;
            case 5:
                det = 21;
                flage = true;
                break;
            case 7:
                det = 9;
                flage = true;
                break;
            case 9:
                det = 3;
                flage = true;
                break;
            case 11:
                det = 19;
                flage = true;
                break;
            case 15:
                det = 7;
                flage = true;
                break;
            case 17:
                det = 19;
                flage = true;
                break;
            case 19:
                det = 11;
                flage = true;
                break;
            case 21:
                det = 5;
                flage = true;
                break;
            case 23:
                det = 17;
                flage = true;
                break;
            case 25:
                det = 25;
                flage = true;
                break;
        }
        return flage;
    }

    function getPlaneText() {
        var temptext = $("#Text").val();
        planeText = temptext.split("");
    }

    function Encryption() {

        // alert(indexes.length);
        for (var i = 0; i < indexes.length; i++) {
            C_indexes[i] =
                (((matrix_input[0]) * (indexes[i])) + (((matrix_input[1])) * (indexes[i + 1]))) % (26);
            // alert(C_indexes[i]);
            C_indexes[i + 1] = (((matrix_input[2]) * (indexes[i])) + (((matrix_input[3])) * (indexes[i + 1]))) %
                (26);
            //  alert(C_indexes[i+1]);
            i++;
        }

        /* for(var i=0; i< C_indexes.length ; i++){
alert(C_indexes[i]);
}*/


        //is even
        if (IsTextisOdd == false) {
            for (var i = 0; i < C_indexes.length; i++) {
                for (var j = 0; j < 26; j++) {
                    if (C_indexes[i] == j) {
                        cypherText[i] = Letters[j];
                    }
                }
            }

            /*    for(var i=0; i< cypherText.length ; i++){
alert(cypherText[i]);
}*/

        } else {
            for (var i = 0; i < (C_indexes.length); i++) {
                for (var j = 0; j < 26; j++) {
                    if (C_indexes[i] == j) {
                        cypherText[i] = Letters[j];
                    }
                }
            }


        }

    }

    function GetLettersIndexesOfText() {

        //is odd
        if (planeText.length % 2 == 0) {
            // alert(planeText.length);
            IsTextisOdd = false;
            for (var i = 0; i < planeText.length; i++) {
                for (var j = 0; j < 26; j++) {
                    if (planeText[i] == (Letters[j])) {
                        indexes[i] = j;
                    }
                }
            }


        } else {
            // alert(planeText.length);
            IsTextisOdd = true;
            //  IsLastTextisOdd=true;
            var i = 0;
            for (; i < planeText.length; i++) {
                for (var j = 0; j < 26; j++) {
                    if (planeText[i] == (Letters[j])) {
                        indexes[i] = j;
                    }
                }
            }
            //add one more (ex : letter z)
            indexes[i++] = 25;
        }
        /* for(var i=0; i< indexes.length ; i++){
alert(indexes[i]);
}*/


    }

    function setCipherText() {

        var temp = "";
        if (IsTextisOdd == false) {
            for (var i = 0; i < cypherText.length; i++) {
                temp += cypherText[i];
            }
            //alert(temp);
            $("#TargetText").text(temp);
        } else {
            for (var i = 0; i < cypherText.length - 1; i++) {
                temp += cypherText[i];
            }
            //alert(temp);
            $("#TargetText").text(temp);
        }


    }


    function calculateInverseMatrix() {

        inverse_matrix[1] = (-1) * (matrix_input[1]);
        inverse_matrix[2] = (-1) * (matrix_input[2]);

        inverse_matrix[0] = matrix_input[3];
        inverse_matrix[3] = matrix_input[0];

        for (var i = 0; i < inverse_matrix.length; i++) {
            if (inverse_matrix[i] < 0) {
                inverse_matrix[i] = ((inverse_matrix[i] % 26) + 26) % 26;
            }
        }

        inverse_matrix[0] = inverse_matrix[0] * (det);
        inverse_matrix[1] = inverse_matrix[1] * (det);
        inverse_matrix[2] = inverse_matrix[2] * (det);
        inverse_matrix[3] = inverse_matrix[3] * (det);

        for (var i = 0; i < inverse_matrix.length; i++) {
            inverse_matrix[i] = (inverse_matrix[i]) % 26;
        }

        /*  for(var i=0; i< inverse_matrix.length ; i++){
// alert(inverse_matrix[i]);
}*/

    }

    function getCipherText() {
        var temptext = $("#Text").val();
        cypherText = temptext.split("");
    }

    function getLettersIndexesoOfCipher() {
        //is odd
        if (cypherText.length % 2 == 0) {

            IsTextisOdd = false;
            for (var i = 0; i < cypherText.length; i++) {
                for (var j = 0; j < 26; j++) {
                    if (cypherText[i] == (Letters[j])) {
                        indexes[i] = j;
                    }
                }
            }


        } else {


            // alert(planeText.length);
            IsTextisOdd = true;

            var i = 0;
            for (; i < cypherText.length; i++) {
                for (var j = 0; j < 26; j++) {
                    if (cypherText[i] == (Letters[j])) {
                        indexes[i] = j;
                    }
                }
            }
            //add one more (ex : letter z)
            indexes[i++] = 25;
        }
        /* for(var i=0; i< indexes.length ; i++){
alert(indexes[i]);
}*/

    }

    function Decryption() {


        for (var i = 0; i < indexes.length; i++) {

            P_indexes[i] = (((inverse_matrix[0]) * (indexes[i])) + (((inverse_matrix[1])) * (indexes[i + 1]))) %
                (26);
            // alert(C_indexes[i]);
            P_indexes[i + 1] =
                (((inverse_matrix[2]) * (indexes[i])) + (((inverse_matrix[3])) * (indexes[i + 1]))) % (26);
            //  alert(C_indexes[i+1]);
            i++;
        }

        /* for(var i=0; i< C_indexes.length ; i++){
alert(C_indexes[i]);
}*/


        //is even
        if (IsTextisOdd == false) {
            for (var i = 0; i < P_indexes.length; i++) {
                for (var j = 0; j < 26; j++) {
                    if (P_indexes[i] == j) {
                        planeText[i] = Letters[j];
                    }
                }
            }

            /*    for(var i=0; i< cypherText.length ; i++){
alert(cypherText[i]);
}*/

        } else {
            for (var i = 0; i < P_indexes.length; i++) {
                for (var j = 0; j < 26; j++) {
                    if (P_indexes[i] == j) {
                        planeText[i] = Letters[j];
                    }
                }
            }
        }

    }

    function setPlaneText() {


        var temp = "";


        if (IsTextisOdd == false) {
            for (var i = 0; i < planeText.length; i++) {
                temp += planeText[i];
            }
            //alert(temp);
            //alert(temp);
        } else {
            for (var i = 0; i < planeText.length - 1; i++) {
                temp += planeText[i];
            }
            //alert(temp);
            //  alert(temp);
        }


        $("#TargetText").text(temp);


    }

});
